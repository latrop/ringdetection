#! /usr/bin/env python


import numpy as np
from random import uniform

try:
    from astropy.io import fits
except ImportError:
    import pyfits as fits


def gaussian2d(xSize, ySize, xCen, yCen, sigma):
    """
    Function returns an (xSize x ySize) array with 2dim gauss function values
    centered at xCen, yCen. The width of the gaussian is governed by
    sigma parameter.
    """
    y = np.arange(xSize)
    x = np.arange(ySize)
    y, x = np.meshgrid(x, y)
    xMin = max(0, int(xCen-5*sigma))
    xMax = min(xSize, int(xCen+5*sigma)+1)
    yMin = max(0, int(yCen-5*sigma))
    yMax = min(ySize, int(yCen+5*sigma)+1)
    closeIdx = np.s_[xMin:xMax, yMin:yMax]
    gauss = np.zeros((xSize, ySize))
    gauss[closeIdx] = np.exp(-((x[closeIdx]-xCen)**2)/sigma - ((y[closeIdx]-yCen)**2)/sigma)
    gauss /= np.sum(gauss)
    return gauss


def image2points(inFitsName, numOfPoints, outFitsName, outShape=(-1, -1)):
    """
    Function modify an input image such that it looks like a result of
    an N-body simulation
    """
    imageData = fits.getdata(inFitsName)
    totalIntens = np.sum(imageData)
    relativeIntens = imageData / totalIntens  # Array contains relative intensities of every pixel normed to a total intens.
    # So, relativeIntens array values shows the fraction of all
    # points, that locate in each pixel
    decPart, intPart = np.modf(numOfPoints * relativeIntens)  # pixelwise number of points
    additinalPoints = np.zeros_like(imageData, dtype=int)
    additinalPoints[np.where(decPart > np.random.random(imageData.shape))] = 1
    pointsArray = np.int_(intPart) + additinalPoints   # each element contains number of pixels with such coordinates
    xSize, ySize = pointsArray.shape
    outData = np.zeros_like(imageData)
    for i in range(xSize):
        for j in range(ySize):
            for k in range(pointsArray[i, j]):
                # replace each point with a gaussian
                x = uniform(i, i+1)  # Random supbixel shift
                y = uniform(j, j+1)  # of a gaussian
                outData += gaussian2d(xSize, ySize, x, y, 1)
    outHDU = fits.PrimaryHDU(data=outData)
    outHDU.writeto(outFitsName, overwrite=True)


if __name__ == "__main__":
    image2points("model_r.fits", 30000, "out.fits")
